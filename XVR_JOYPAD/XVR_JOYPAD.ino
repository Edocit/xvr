/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Authors : Edoardo  Cittadini                                                                                                                             //
//          Gerlando Sciangula                                                                                                                             //
//                                                                                                                                                         //
//                                                                                                                                                         //
//                                    |--------------------------------------------------------------------------------|                                   //
//                                    |                                                                                |                                   //
//                                    |       O           O             |--|--|--|                      O              |                                   //
//                                    |                                 |        |                  O       O          |                                   //
//                                    |                                 |        |                      O              |                                   //
//                                    |                                 |  NANO  |                                     |                                   //
//                                    |         |-------|               |        |                  |-------|          |                                   //
//                                    |         |   O   |               |        |                  |   O   |          |                                   //
//                                    |         |-------|               |--------|                  |-------|          |                                   //
//                                    |          ANALOG_L                                            ANALOG_R          |                                   //
//                                    ---------------------------------------------------------------------------------|                                   //
//                                                                                                                                                         //
///////////////////////////////////////////////////////////////////////////MACROS////////////////////////////////////////////////////////////////////////////
#define CONNECTION_PDU  10                                                                                                                                 //
#define JOYPAD          "2"                                                                                                                                //
#define CLOSE_COM       3                                                                                                                                  //
#define COMMANDS_NR     12                                                                                                                                 //
#define PERIOD          100000    //10 Hz in us (microsecondi)                                                                                             //
/////////////////////////////////////////////////////////////////////VARIABILI GLOBALI///////////////////////////////////////////////////////////////////////
uint16_t      commands[COMMANDS_NR];                                                                                                                       //
String        string_to_send;                                                                                                                              //
unsigned long time_cnt;                                                                                                                                    //
bool          is_com_binded;                                                                                                                               //
enum          hw_cmd{ ANALOG_RY, ANALOG_RX, ANALOG_LY, ANALOG_LX, CHANGE_MODE, EXIT_BTN, CHANGE_WEAPON, SHOOT, RELOAD, JUMP, ANALOG_L_BTN, ANALOG_R_BTN }; //
/////////////////////////////////////////////////////////////////////////PROTOTIPI///////////////////////////////////////////////////////////////////////////
void com_handshake(void);                                                                                                                                  //
void synch(void);                                                                                                                                          //
void wait_for_next_activation(void);                                                                                                                       //
void parse_commands(void);                                                                                                                                 //
void send_commands(void);                                                                                                                                  //
void check_for_reset(void);                                                                                                                                //
                                                                                                                                                           //
void(* resetFunc)(void) = 0;    //funzione HW del core che permette il RESET in modo equivalente alla pressione del bottone sulla scheda                   //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup(){
  pinMode(A0, INPUT);           //si poteva evitare perchè di default tutti i pin sono impostati come input, lo esplicitiamo per maggior charezza
  pinMode(A1, INPUT);           //si poteva evitare perchè di default tutti i pin sono impostati come input, lo esplicitiamo per maggior charezza
  pinMode(A2, INPUT);           //si poteva evitare perchè di default tutti i pin sono impostati come input, lo esplicitiamo per maggior charezza
  pinMode(A3, INPUT);           //si poteva evitare perchè di default tutti i pin sono impostati come input, lo esplicitiamo per maggior charezza
  
  pinMode(5,  INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto
  pinMode(6,  INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto
  pinMode(7,  INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto
  pinMode(8,  INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto
  pinMode(9,  INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto
  pinMode(10, INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto
  pinMode(11, INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto
  pinMode(12, INPUT_PULLUP);    //il pin è collegato in modalità INPUT_PULLUP per evitare letture casuali quando non premuto

  Serial.begin(9600);           //comunicazione seriale classica 8N1 con Baud rate = 9600 
  is_com_binded = false;        //imposto la seriale alla modalità non connessa
  com_handshake();              //aspetto che il programma chiamante mi mandi il messaggio di connessione sulla porta seriale
}


void loop(){
  synch();                      //sincronizzo il loop a inizio periodo
  check_for_reset();            //controllo se ho ricevuto il segnale di RESET
  parse_commands();             //leggo i comandi e preparo il vettore
  send_commands();              //trasformo il vettore in una stringa CSV (Comma Separated Values)
  wait_for_next_activation();   //Rimango in attesa della fine del periodo per procedere col ciclo successivo 
}





/* Rimango in attesa nel setup fino a quando un programma chiamante non si collega sulla porta;   *
 * il programma chiamante ha la possibilità di scoprire questo dispositivo leggendo sulla seriale *
 * il codice 2 che viene ripetutamente inviato come byte di handshake dal Nano; quando il master  *
 * trova la seriale su cui il Nano è in ascolto manda a sua volta il codice 2 come segnale di     *
 * avvenuto collegamento e piena disponibilità a riceve dati su quel canale. Se tutto va a buon   *
 * fine il Nano legge 2 sulla seriale, imposta la porta come connessa, interrompe questa funzione *
 * ed entra nel loop() principale                                                                 */
void com_handshake(){
  while(is_com_binded == false){
    if(Serial.available() > 0){
      if(Serial.read() | 00000010 == (byte)2){ 
        is_com_binded = true;
        break;
      }
    }else{
      Serial.print(JOYPAD);
    }
    while(micros()- time_cnt < PERIOD){};
    time_cnt = micros();
  }
}




//Prende il tempo di inizio ciclo per la sincronizzazione sul periodo del loop 
void synch(){
  time_cnt = micros();
}



/* In base al valore della macro PERIOD impostata a inizio programma regola *
 * il periodo del loop per l'invio alla frequenza desiderata                */
void wait_for_next_activation(){
  while( (micros() - time_cnt) < PERIOD){};
}



//Legge i device di input e salva i valori nel vettore dei comandi
void parse_commands(){
  commands[ANALOG_RY]     = analogRead(A0);
  commands[ANALOG_RX]     = analogRead(A1);
  commands[ANALOG_LY]     = analogRead(A2);
  commands[ANALOG_LX]     = analogRead(A3); 
  
  commands[CHANGE_MODE]   = digitalRead(5);
  commands[EXIT_BTN]      = digitalRead(6);
  commands[CHANGE_WEAPON] = digitalRead(7);  
  commands[SHOOT]         = digitalRead(8);
  commands[RELOAD]        = digitalRead(9);
  commands[JUMP]          = digitalRead(10);
  commands[ANALOG_L_BTN]  = digitalRead(11);
  commands[ANALOG_R_BTN]  = digitalRead(12);
}


/* Converte il vettore dei comandi in una stringa in cui i comandi sono separati da virgola (CSV) *
 * ed la invia sulla seriale al programma chiamante                                               */
void send_commands(){
  for(uint8_t i = 0; i < COMMANDS_NR; i++){
    string_to_send.concat(String(commands[i])+",");
  }

  string_to_send.remove(string_to_send.length()-1);  //elimino l'ultima virgola
  Serial.println(string_to_send);
  string_to_send = "";
}


/* Una volta per ciclo, e quindi se non sono stati modificati i parametri di default 10 volte al secondo, *
 * si controlla se il programma chiamante ha inviato un segnale di RESET a questa periferica; se si viene *
 * chiamata la funzione HW di reset (descritta nei prototipi all'inizio di questo file) ed il programma   *
 * riparte da zero come se fosse la prima accensione. Questo è necessario tutte le volte che un programma *
 * che deve utilizzare la periferica perde la consapevolezza/non conosce la porta seriale su cui questa è *
 * collegata                                                                                              */
void check_for_reset(){
  if(Serial.available() > 0){
    if(Serial.read() | 00000011 == (byte)(CLOSE_COM)){ 
      resetFunc();
    }
  }
}
