
import os
import socket    
#os.system('pip install PySimpleGUI')
import PySimpleGUI as sg


IPAddr    = ""
MAX_LEN   = 30
IP_FIELDS = 4


def get_ip():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	try:
		s.connect(('10.255.255.255',1))
		IP = str(s.getsockname()[0])
	except:
		IP = '127.0.0.1'
	finally:
		s.close()

	return IP




IPAddr = get_ip()

sg.theme('DarkAmber')   # Add a touch of color
# All the stuff inside your window.
layout = [  [sg.Text('Insert your game data')],
            [sg.Text('Nickname '), sg.InputText()],
            [sg.Text('IP address'), sg.InputText(IPAddr)],
            [sg.Text('*Change your IP only if you use a VPN connection')],
            [sg.Button('Ok')] ]

# Create the Window
window = sg.Window('XVR Client data', layout)
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event in (None, 'Ok'):
      f = open("client_data.txt", "w")

      if(event == "Ok"):
        if("," not in values[0] and len(values[1].split(".")) == IP_FIELDS and len(values[0]) != 0):
          if(len(values[0]) > MAX_LEN):
            f.write(str(values[0][:MAX_LEN])+","+str(values[1]))
          else:
            f.write(str(values[0])+","+str(values[1]))
          break
      else:
        f.write("")
        break
      
      f.close()


window.close()

