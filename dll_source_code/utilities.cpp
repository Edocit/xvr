// utilities.cpp : Definisce le funzioni esportate per la DLL.
//



#include "pch.h"
#include "framework.h"
#include "utilities.h"
#include <stdio.h>
#include <winsock.h>
#include <math.h>
//#include <stdio.h>
#include <stdlib.h>


#pragma comment(lib,"ws2_32.lib")   //Winsock Library

#define BUFLEN			   1024		//Max length of buffer
#define SERVER_PORT       50023		//The port on which to listen for incoming data
enum block_type { BLOCKING, NO_BLOCKING };


extern "C" __declspec(dllexport) void delay_ms(int ms) {
	ms = abs(ms);
	Sleep(ms);
}


//Ritorna l'indirizzo IP del client
extern "C" __declspec(dllexport) char* get_IPaddress() {
	FILE *fp;
	char ip[40];

	fp = _popen("ipconfig", "r");

	while (fgets(ip, sizeof(ip), fp) != NULL){
		printf("%s", ip);
	}

	_pclose(fp);

	return ip;
}



extern "C" __declspec(dllexport) void get_client_data() {
	system("client_form.exe");
}



//Create the socket on specific invocation, same as NetCreateChannel 
extern "C" __declspec(dllexport) int create_socket(char* client_ip, int client_port) {
	struct sockaddr_in si_other,si_client;
	int s, slen = sizeof(si_other);
	WSADATA wsa;

	//Initialise winsock
	WSACleanup();
	//printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) { return -1; }

	//create socket
	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR) { return -2; }

	//setup address structure
	memset((char*)&si_client, 0, sizeof(si_client));
	si_client.sin_family = AF_INET;
	si_client.sin_port = htons(client_port);
	si_client.sin_addr.S_un.S_addr = inet_addr(client_ip);

	//Bind
	if (bind(s, (struct sockaddr*) & si_client, sizeof(si_client)) == SOCKET_ERROR) {
		closesocket(s);
		WSACleanup();
		return -3;
	}


	return s;
}





//Close the socket on specific invocation 
extern "C" __declspec(dllexport) void close_socket(int s) {
	closesocket(s);
	WSACleanup();
}






//If it returns true, your next call to recv should return immediately with some data.
bool readyToReceive(int sock, int interval){
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(sock, &fds);

	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = interval;

	return (select(sock + 1, &fds, 0, 0, &tv) == 1);
}






extern "C" __declspec(dllexport) char* read_incoming_pdu(char* server_ip, int s, int call_type) {
	struct sockaddr_in si_other;
	int slen = sizeof(si_other);
	char buf[BUFLEN];


	//setup address structure
	memset((char*)&si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(SERVER_PORT);
	si_other.sin_addr.S_un.S_addr = inet_addr(server_ip);

	//receive a reply and print it
	//clear the buffer by filling null, it might have previously received data
	memset(buf, '\0', BUFLEN);

	//non blocking call 
	if (call_type == NO_BLOCKING) {
		if (readyToReceive(s, 1)) { //5us
			//try to receive some data, this is a blocking call
			if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr*) & si_other, &slen) == SOCKET_ERROR){
				return (char*)("-4");
			}
		}
		else {
			return (char*)"";
		}
	}

	//blocking call
	if (call_type == BLOCKING) {
		if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr*) & si_other, &slen) == SOCKET_ERROR) { 
			return (char*)("-4");
		}
	}



	return buf;
}

