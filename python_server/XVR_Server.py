
import socket
import random
import time
import requests
import threading
from threading import Thread
import os



POWERUP_TIME         =    40
WAITING_TIME         =     5  
last_new_player_time =     0
started_match        = False #questa variabile è molto importante perchè non consente il join di altri players a partita già iniziata ( viene usata in parse_pdu() )
powerup_thread       =     0 
time_elapsed         =     0

''' Questa funzione serve perchè la receive del Server UDP  è  bloccante  perciò  per  avere  un  time  to  match 
    dipendente solo  dal tempo passato dall' entrata dell' ultimo giocatore,   gestiamo  questo  timer  in   modo
    concorrente  utilizzando un thread che viene terminato all'inizio della partita  e  restituisce  il controllo
    all'oggetto server principale (XVR_Server) che lo aveva creato per tutto il resto della partita              '''

def ready_thread_run(server):
    global last_new_player_time
    global WAITING_TIME
    global powerup_thread

    while(True):
        if( time.time() - last_new_player_time >= WAITING_TIME ):
            started_match = True

            server.packet_from_server  = server.READY_PDU + ","

            for i in range(0,len(server.player_address)):
                server.packet_from_server += str(server.player_id[i]) + "," + server.player_color[i] + "," + server.player_nickname[i] + ","
            
            server.packet_from_server = server.packet_from_server[:len(server.packet_from_server) - 1]  #elimina la virgola finale
            print("Nel ready thread --> ", server.packet_from_server)
            bytesToSend = server.packet_from_server.encode()
            
            for i in range(0,len(server.player_address)):
                server.UDPServerSocket.sendto(bytesToSend, server.player_address[i])

            #prima di terminare passa il controllo all'altro thread che gestisce i powerups
            powerup_thread = Thread(target=powerup_thread_run, args=(server,))
            powerup_thread.start()

            return
        
        #print("Time to start game: ", WAITING_TIME - int(time.time() - last_new_player_time))
        time.sleep(0.2)
        #os.system("clear")



''' Questa funzione è il corpo del thread che ogni POWERUP_TIME manda un pacchetto per istanziare un powerup 
    nella scena per un tempo specificato nel pacchetto stesso                                                '''

def powerup_thread_run(server):
    global POWERUP_TIME
    global time_elapsed
    while(True):
        if(time_elapsed % POWERUP_TIME == 0):
            pu_type = str(random.randint(0,1))
            pu_time = str(random.randint(30,50))

            server.packet_from_server  = server.POWERUP_PDU + "," + server.player_random_spawn() + ","
            server.packet_from_server += pu_type + "," + pu_time
            print(server.packet_from_server)
            bytesToSend = server.packet_from_server.encode()

            for i in range(0,len(server.player_address)):
                if(server.player_id[i] != server.REMOVED):
                    server.UDPServerSocket.sendto(bytesToSend, server.player_address[i])
        
        time_elapsed = time_elapsed + 2
        
        if(server.players_in_game <= 1):
            print("Returned powerup thread")
            return
        else:
            time.sleep(2)






class XVR_Server():
####################################SERVER CONSTANTS##################################
    INIT_PDU         = "0"
    PLAYER_PDU       = "1"
    BULLET_PDU       = "2"
    POWERUP_PDU      = "3"
    BULLET_DEL_PDU	 = "4"
    RETRY_PDU        = "5"
    EXIT_PDU         = "6"
    READY_PDU        = "7"
    WIN_PDU          = "8"     
    DEL_POWERUP_PDU  = "9"
    localIP          = "25.102.35.65"
    GUI_SERVER_HTTP  = "http://127.0.0.1:8000/" #andamo direttamente su Chrome e usiamo tutta la potenza di Django,Bootstrap,HTML5,CSS,JavaScript,AJAX
    OK               =   200                    #codice "OK" di risposta dal GUI Server per i dati del player inviati 
    localPort        = 50023
    client_port      = 50025
    MAX_BYTES        =  1024
    IP               =     0                #serve per indicizzare la tupla in modo chiaro
    PORT             =     1                #serve per indicizzare la tupla in modo chiaro
    REMOVED          =    -1
    colors           = [ "1,0,0", "0,1,0", "0,0,1", "1,0,1", "1,1,0", "0,1,1", "1,1,1" ]
    MED_PU           =     0       
    AMM_PU           =     1
#####################################SERVER PRIVATE DATA##############################
    MAX_PLAYERS          = 2
    next_player_id       = 0
    players_in_game      = 0
    player_nickname      = []
    player_position      = []
    player_color         = []
    player_address       = []
    player_id            = []
    packet_from_server   = ""
    packet_from_client   = []
    UDPServerSocket      = None

    f = [   [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,1,2,3,4,5,6,5,4,3,2,2,2,1,0,0,0,0,7,7,7,7,7,7,7,7,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,2,2,5,5,5,5,5,5,5,2,2,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,7,0,0,0,1,1,1,2,2,3,3,4,5,4,3,3,2,2,1,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,2,2,3,3,3,3,3,3,3,2,2,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,2,2,3,3,3,3,3,3,3,2,2,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,7],
            [7,0,0,0,7,7,7,7,7,7,7,7,0,0,0,0,1,0,0,0,0,7,7,7,7,7,7,7,7,7,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7],
            [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7] ]


    #######################################CONSTRUCTOR#####################################
    def __init__(self):
        # Creo un socket UDP Server-side datagram oriented
        self.UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

        #Collego il server con ip e porta
        self.UDPServerSocket.bind((self.localIP, self.localPort))
        print("Binded succesfully to IP : " + str(self.localIP) + "  PORT : " + str(self.localPort))

        print("UDP server up and listening")
    ########################################FUNCTIONS######################################

    def restart(self):
        global last_new_player_time
        global started_match
        global powerup_thread

        powerup_thread.join()
        print("Server was succesfully restarted")
        print("IP : " + str(self.localIP) + "  PORT : " + str(self.localPort))
        self.MAX_PLAYERS          = 2
        self.next_player_id       = 0
        self.players_in_game      = 0
        self.player_nickname      = []
        self.player_position      = []
        self.player_color         = []
        self.player_address       = []
        self.player_id            = []
        self.packet_from_server   = ""
        self.packet_from_client   = [] 
        last_new_player_time      = 0
        started_match             = False



    ''' La funzione inizializzza a caso la posizione controllando la level_matrix    
        ed evitando tutte le posizioni in cui ci sono muri, tentando a caso la prima 
        posizione buona viene assegnata come posizione di partenza del giocatore     '''
    def player_random_spawn(self):

        num_rows    = len(self.f)
        num_columns = len(self.f[0])

        x     = 0
        y_adj = [0,0,0,0,0,0,0,0,0]
        z     = 0

        retry = True

        while(retry):
            x = random.randint(0,num_columns-3) + 1
            z = random.randint(0,num_rows-3) + 1
            
            y_adj[0] = self.f[z-1][x]
            y_adj[1] = self.f[z-1][x-1]
            y_adj[2] = self.f[z-1][x+1]
            y_adj[3] = self.f[z][x-1]
            y_adj[4] = self.f[z][x+1]
            y_adj[5] = self.f[z+1][x-1]
            y_adj[6] = self.f[z+1][x]
            y_adj[7] = self.f[z+1][x+1]
            y_adj[8] = self.f[z][x]
            
            retry = False
            
            for i in range(9):
                if(y_adj[i] > 0):
                    retry = True
            
            if(retry == False):
                if([x,0,z] not in self.player_position):    #se la posizione casuale non è gia stata assegnata ad un altro giocatore
                    self.player_position.append([x,0,z])    #assegno la nuova posizione e la salvo per controlo=lare che non sia data anche ad altri 

        return str(x)+",0,"+str(z)




    def assign_id(self):
        id = str(self.next_player_id)
        self.next_player_id += 1

        return id




    def assign_color(self):
        idx = random.randint(0,len(self.colors)-1)
        col = self.colors[idx]
        self.player_color.append(col)
        #del self.colors[idx]

        return col




    def register_new_client(self, pdu, net_data):
        self.player_nickname.append(pdu.split(",")[1]) # PDU --> "header,nickname".split(",") = [header, nickname] <-> [1] = nickname 
        print(self.player_nickname)
        self.player_id.append(self.assign_id())
        self.player_address.append( (net_data[self.IP],self.client_port) )       
        #print(self.player_address)



    def forward_to_gui_server(self, bytesToSend):
        try:
            data_to_gserver = { 'data' : bytesToSend.decode() }
            r = requests.get(self.GUI_SERVER_HTTP, data_to_gserver)
            r = int(str(r).split("[")[1].split("]")[0])

            if(r != self.OK):
                print("Error from GUI Server")  

        except:
            print("GUI Server not connected")



    def init_client_pdu(self):
        global last_new_player_time

        self.packet_from_server  = self.INIT_PDU    + "," + self.player_random_spawn() + ","
        self.packet_from_server += str(self.player_id[len(self.player_address)-1]) + "," + self.assign_color()

        bytesToSend = self.packet_from_server.encode()

        self.UDPServerSocket.sendto(bytesToSend, self.player_address[len(self.player_address)-1] )
        self.forward_to_gui_server(bytesToSend)
        self.players_in_game += 1

        print("PL --> ", self.players_in_game)

        if(self.players_in_game > 1):
            last_new_player_time = time.time()  #aggiorno la variabile utilizzata anche da thread
            
            if(self.players_in_game == 2):
                ready_thread = Thread(target=ready_thread_run, args=(self,))
                ready_thread.start()

            print("Sono andato avanti")





    def forward_player_pdu(self, pdu):
        p_id = int(pdu[2])
        
        bytesToSend = pdu.encode()
        #print(bytesToSend)
        
        for i in range (0, len(self.player_address)):
                if(p_id != i and self.player_id[i] != self.REMOVED):
                    self.UDPServerSocket.sendto(bytesToSend, self.player_address[i] )




    def forward_bullet_pdu(self, pdu):
        temp = pdu.split(",")
        #print(temp)
        p_id = int(temp[7])

        bytesToSend = pdu.encode()
        #print(bytesToSend)
        
        for i in range (0, len(self.player_address)):
            if(p_id != i and self.player_id[i] != self.REMOVED):
                self.UDPServerSocket.sendto(bytesToSend, self.player_address[i])





    def forward_bullet_del_pdu(self,pdu):
        tmp = pdu.split(",")
        p_id = int(tmp[3])

        bytesToSend = pdu.encode()
        
        
        for i in range (0, len(self.player_address)):
                if(p_id != i and self.player_id[i] != self.REMOVED):
                    self.UDPServerSocket.sendto(bytesToSend, self.player_address[i])
                   # print(bytesToSend.decode() + "      " + str(self.player_address[i]))



    ''' Elimina un giocatore spedendogli indietro un PDU di conferma e viene marcato come REMOVED nelle tabelle
        dinamiche del server che poi sfrutterà tale dato per informare anche gli altri clients della rimozione
        dell'utente specifico                                                                                    '''
    def remove_client(self,pdu):
        bytesToSend = self.EXIT_PDU.encode()

        if(self.players_in_game > 0):
            self.UDPServerSocket.sendto(bytesToSend, self.player_address[int(pdu[2])])
            self.player_address[int(pdu[2])] = self.REMOVED            
            self.player_id[int(pdu[2])]      = self.REMOVED
            self.players_in_game -= 1
        
            self.has_a_winner()



    def forward_powerup_del_pdu(self, pdu, address):
        bytesToSend = pdu.encode()
        address = (list(address)[0], self.client_port)

        for i in range(0, len(self.player_address)):
            if(self.player_address[i] != address and self.player_address[i] != self.REMOVED):
                self.UDPServerSocket.sendto(bytesToSend, self.player_address[i])
        



    def has_a_winner(self):
        if(self.players_in_game == 1):
            for i in range(0,len(self.player_id)):
                if(self.player_id[i] != self.REMOVED):
                   bytesToSend = self.WIN_PDU + "," + str(self.player_id[i])
                   bytesToSend = bytesToSend.encode()
                   self.UDPServerSocket.sendto(bytesToSend, self.player_address[i])   

        if(self.players_in_game == 0):
            self.restart()




    def parse_pdu(self, pdu, net_data):
        global started_match                                    #variabile globale usata e condivisa col thread
        pdu = pdu.decode()                                      #trasformo l'array di bytes in stringa decodificata in UTF-8

        if(pdu[0] == self.INIT_PDU and started_match == False):
            self.register_new_client(pdu, net_data)             #aggiungo il nuovo client alla lista dei clients in partita
            self.init_client_pdu()                              #rispondo al nuovo client inviandogli i suoi dati per la partita
            return                                              #l'inizio della partita è decretato dal thread

        if(pdu[0] == self.PLAYER_PDU):
            self.forward_player_pdu(pdu)                        #manda il PDU a tutti i players tranne a quello che l'ha spedito (per ovvie ragioni)
            return

        if(pdu[0] == self.POWERUP_PDU):                        #manda il PDU dello spawn del powerup
            return

        if(pdu[0] == self.BULLET_PDU):
            self.forward_bullet_pdu(pdu)                        #manda info relative ad un nuovo proiettile sparato a tutti gli altri players
            return

        if(pdu[0] == self.BULLET_DEL_PDU):
            self.forward_bullet_del_pdu(pdu)                    #manda info relative ad un proiettile da eliminare dalla scena a tutti gli altri players
            return
        
        if(pdu[0] == self.DEL_POWERUP_PDU):
            self.forward_powerup_del_pdu(pdu, net_data)
            return

        if(pdu[0] == self.EXIT_PDU):
            self.remove_client(pdu)                             #elimina un client, gli da un ACK e lo marca come rimosso nelle tabelle dinamiche
            return
                     


    def run(self):
        #Ciclo infinito in ascolto di dati in ingresso 
        while(True):
            message, address = self.UDPServerSocket.recvfrom(self.MAX_BYTES)
            self.parse_pdu(message, address)
        




if(__name__ == "__main__"):
    try:
        server = XVR_Server()
        server.run()
    except KeyboardInterrupt:
        #mandare Server_Crash_PDU
        print("Server is going to disconnect")